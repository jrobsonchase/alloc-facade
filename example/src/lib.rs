#![cfg_attr(feature = "alloc", feature(alloc))]
#![cfg_attr(not(feature = "std"), no_std)]

#![feature(futures_api)]

use alloc::format;

use alloc::prelude::*;

fn hello(name: &str) -> String {
    format!("hello, {}!", name)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
